## LARAVEL 8 + DOCKER(Apache, php7.4 + nginx + msql) + FIREBASE ##

## Comandos
docker-compose up -d

## Entrar no bash
docker compose exec app bash

## Parar docker
docker-compose stop <br />
docker-compose down <br />
docker kill $(docker ps -q)

## Apagar imagens
docker system prune -a

## Visualizar imagens
docker images

## Gerar key
php artisan key:generate

# Para configura  do zero, vc tem que entrar dentro do BASH e execulta o COMPOSER INSTALL para gerar vendor.
1- docker-compose up -d <br />
2- docker compose exec app bash <br />
3- composer install 

## URL PARA ABRIR PROJETO
http://dev.firebase.com:8000/

## NGINX
vhost é configurando dentro de /docker/nginx/default.conf

##by Anthoni##